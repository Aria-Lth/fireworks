const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const passport = require("passport");

router.post("/login", (req, res) => {
    passport.authenticate("local", { session: false }, (err, user, info) => {
        if (err) {
            return res.status(500).json({ message: info.message });
        } else if (!user) {
            return res.status(401).json({ message: info.message });
        }
        req.login(user, { session: false }, (err) => {
            if (err) {
                res.status(500).json({ message: err });
            }
            const token = jwt.sign(user, process.env.JWT_SECRET);
            return res.status(200).json({ token, message: info.message });
        });
    })(req, res);
});

module.exports = router;
