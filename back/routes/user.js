const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const connection = require("../conf");
const passport = require("passport");

const routeName = "/user";

router.get(
    routeName + "/list",
    passport.authenticate("jwt", {}),
    (req, res) => {
        connection.query(`SELECT * FROM users`, (err, result) => {
            if (err) {
                console.log(err);
                res.status(500).send("Error...");
            }
            res.status(200).json(result);
        });
    }
);

router.post(routeName + "/signin", (req, res) => {
    const formData = req.body;
    formData.password = bcrypt.hashSync(formData.password, 10);
    const values = Object.values(formData);
    connection.query(
        `INSERT INTO users \
            (name, email, password) \
            VALUES (${"?, ".repeat(values.length).slice(0, -2)})`,
        values,
        (err) => {
            delete formData.password;
            if (err) {
                res.status(500).send(err).json({
                    message: "Erreur lors de l'inscription...",
                    body: formData,
                    toastType: "error",
                });
            } else {
                res.status(201).json({
                    message: "Inscription réussie !",
                    body: formData,
                });
            }
        }
    );
});

module.exports = router;
