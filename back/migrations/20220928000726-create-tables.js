"use strict";

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

exports.up = async function (db, callback) {
    db.createTable(
        "users",
        {
            id: { type: "int", primaryKey: true, autoIncrement: true },
            name: { type: "string", unique: true },
            email: { type: "string", unique: true },
            password: "string",
        },
        callback
    );
    db.createTable(
        "maps",
        {
            id: { type: "int", primaryKey: true, autoIncrement: true },
            name: { type: "string", unique: true },
        },
        callback
    );
    db.createTable(
        "units",
        {
            id: { type: "int", primaryKey: true, autoIncrement: true },
            name: { type: "string", unique: true },
        },
        callback
    );
};

exports.down = function (db, callback) {
    db.dropTable("users", callback);
    db.dropTable("maps", callback);
    db.dropTable("units", callback);
    return null;
};

exports._meta = {
    version: 1,
};
