"use strict";

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
};

exports.up = function (db, callback) {
    // [
    //     ["test1", "test1@t.com", "t1pwd"],
    //     ["test2", "test2@t.com", "t2pwd"],
    // ].map((data) =>
    //     db.insert("users", ["name", "email", "password"], data, callback)
    // );
    db.runSql("INSERT INTO \
        users (name, email, password) VALUES \
        ('test1', 't1@test.com', 'test1pwd'), \
        ('test2', 't2@test.com', 'test2pwd') \
    ;", callback)
};

exports.down = function (db) {
    return null;
};

exports._meta = {
    version: 1,
};
