const path = require("path");
require("dotenv").config({ path: path.resolve(__dirname, "./.env") });
const express = require("express");
const app = express();
const port = 8000;
require("./passport-strategy");

const routes = require("./routes");

app.use(express.json());

app.get("/", (req, res) => {
    res.send("Ok !");
});

routes.forEach((r) => app.use("/api", r));

app.listen(port, (err) => {
    if (err) {
        console.error(err);
        throw new Error("Something Bad Happened ...");
    }
    console.log(`server is listening on ${port}`);
});
