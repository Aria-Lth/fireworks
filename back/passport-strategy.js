const passport = require("passport");
const connection = require("./conf");
const LocalStrategy = require("passport-local").Strategy;
const passportJWT = require("passport-jwt");
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const bcrypt = require("bcrypt");

passport.use(
    new LocalStrategy(
        {
            usernameField: "email",
            passwordField: "password",
        },
        function (email, password, cb) {
            connection.query(
                `SELECT * FROM users WHERE email = ?`,
                [email],
                (err, result) => {
                    if (err) {
                        return cb(null, false, {
                            message: "Erreur lors de la connexion...",
                            error: err,
                        });
                    } else if (result.length === 0) {
                        return cb(null, false, {
                            message:
                                "Cette adresse e-mail n'est pas enregistrée.",
                        });
                    }
                    if (bcrypt.compareSync(password, result[0].password)) {
                        return cb(
                            null,
                            {
                                email: result[0].email,
                                name: result[0].name,
                            },
                            {
                                message: "Connexion réussie !",
                            }
                        );
                    } else {
                        return cb(null, false, {
                            message: "Mot de passe incorrect.",
                        });
                    }
                }
            );
        }
    )
);

passport.use(
    new JWTStrategy(
        {
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.JWT_SECRET,
        },
        function (jwtPayload, cb) {
            return cb(null, jwtPayload);
        }
    )
);
