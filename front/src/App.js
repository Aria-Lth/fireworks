import { useContext } from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import Login from "./Login.js";
import SignIn from "./SignIn.js";
import Home from "./Home.js";
import { AuthContext } from "./AuthContext.js";

const App = () => {
    const { loginStatusState } = useContext(AuthContext);
    return (
        <>
            <p>OK</p>
            <Routes>
                {!loginStatusState.isLoggedIn ? (
                    <>
                        <Route path="/login" element={<Login />} />
                        <Route path="/signin" element={<SignIn />} />
                        <Route
                            path="*"
                            element={<Navigate to="/login" replace />}
                        />
                    </>
                ) : (
                    <>
                        <Route path="/home" element={<Home />} />
                        <Route
                            path="*"
                            element={<Navigate to="/home" replace />}
                        />
                    </>
                )}
            </Routes>
        </>
    );
};

export default App;
