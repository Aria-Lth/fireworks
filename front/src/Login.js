import { useContext } from "react";
import { useSetState } from "react-use";
import { Button } from "reactstrap";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "./AuthContext.js";

const initialLoginState = {
    email: "",
    password: "",
};

const Login = () => {
    const navigate = useNavigate();
    const { loginStatusState: ContextState, login } = useContext(AuthContext);
    const { isLoginPending, loginError } = ContextState;
    const [loginState, setLoginState] = useSetState(initialLoginState);

    const onSubmit = (e) => {
        e.preventDefault();
        const { email, password } = loginState;
        login(email, password);
    };

    return (
        <>
            <h1>Connexion</h1>
            <form name="loginForm" onSubmit={onSubmit}>
                <label htmlFor="email">Email</label>
                <input
                    type="text"
                    name="email"
                    onChange={(e) => setLoginState({ email: e.target.value })}
                    value={loginState.email}
                />
                <label htmlFor="password">Mot de passe</label>
                <input
                    type="password"
                    name="password"
                    onChange={(e) => setLoginState({ password: e.target.value })}
                    value={loginState.password}
                />
                <input type="submit" value="Se connecter" />
                {isLoginPending && <p>Veuillez patienter...</p>}
                {loginError && <p style={{color: "red"}}>{loginError}</p>}
            </form>
            <Button color="secondary" onClick={() => navigate("/signin")}>
                S'inscrire
            </Button>
        </>
    );
};

export default Login;
