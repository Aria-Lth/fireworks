import { useContext } from "react";
import { AuthContext } from "./AuthContext.js";
import { Link } from "react-router-dom";

const Home = () => {
    const { logout } = useContext(AuthContext);
    const onLogout = (e) => {
        e.preventDefault();
        logout();
    };

    return (
        <>
            <h1>Fireworks!!</h1>
            <Link href="/login" onClick={onLogout}>
                Logout
            </Link>
            <Link href="/login">
                test
            </Link>
        </>
    );
};

export default Home;
