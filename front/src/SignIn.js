import { useState } from "react";
import { useSetState } from "react-use";
import { Button } from "reactstrap";
import { useNavigate } from "react-router-dom";

const initialState = {
    name: "",
    email: "",
    password: "",
};

const SignIn = () => {
    const navigate = useNavigate();
    const [state, setState] = useSetState(initialState);
    const [reqMsg, setReqMsg] = useState("");
    const [reqStatus, setReqStatus] = useState("");

    const onSubmit = (e) => {
        e.preventDefault();
        fetch("/user/signin", {
            method: "POST",
            body: JSON.stringify(state),
            headers: {
                Accept: "application/json, text/plain",
                "Content-Type": "application/json",
            },
        })
            .then((res) => {
                if (res.status !== 201) {
                    setReqStatus("error");
                } else {
                    setReqStatus("");
                }
                return res.json();
            })
            .then((res) => {
                setReqMsg(res.message);
            });
        setState({
            name: "",
            email: "",
            password: "",
        });
    };

    return (
        <>
            <h1>Inscription</h1>
            <form name="signInForm" onSubmit={onSubmit}>
                <label htmlFor="name">Nom</label>
                <input
                    type="text"
                    name="name"
                    onChange={(e) => setState({ name: e.target.value })}
                    value={state.name}
                />
                <label htmlFor="email">Email</label>
                <input
                    type="text"
                    name="email"
                    onChange={(e) => setState({ email: e.target.value })}
                    value={state.email}
                />
                <label htmlFor="password">Mot de passe</label>
                <input
                    type="password"
                    name="password"
                    onChange={(e) => setState({ password: e.target.value })}
                    value={state.password}
                />
                <input type="submit" value="S'inscrire" />
            </form>
            {reqMsg && (
                <p style={{ color: reqStatus === "error" && "red" }}>
                    {reqMsg}
                </p>
            )}
            <Button color="secondary" onClick={() => navigate("/login")}>
                Se connecter
            </Button>
        </>
    );
};

export default SignIn;
