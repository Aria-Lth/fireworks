import { createContext } from "react";
import { useSetState } from "react-use";

export const AuthContext = createContext(null);

const initialLoginStatusState = {
    isLoggedIn: false,
    isLoginPending: false,
    loginError: null,
};

const initialUserState = {
    name: "",
};

export const ContextProvider = (props) => {
    const [loginStatusState, setLoginStatusState] = useSetState(
        initialLoginStatusState
    );
    const [userState, setUserState] = useSetState(initialUserState);

    const setLoginPending = (isLoginPending) =>
        setLoginStatusState({ isLoginPending });
    const setLoginSuccess = (isLoggedIn) => setLoginStatusState({ isLoggedIn });
    const setLoginError = (loginError) => setLoginStatusState({ loginError });

    const login = (email, password) => {
        setLoginPending(true);
        setLoginSuccess(false);
        setLoginError(null);

        fetchLogin(email, password)
            .then((res) => {
                setLoginPending(false);
                if (res.token) {
                    localStorage.setItem("fireworksUser", res.token);
                    setLoginSuccess(true);
                } else {
                    setLoginError(res.message);
                }
            })
            .catch((e) => {
                setLoginPending(false);
                setLoginError(e);
            });
    };

    const logout = () => {
        localStorage.removeItem("fireworksUser");
        setLoginPending(false);
        setLoginSuccess(false);
        setLoginError(null);
        setUserState();
    };

    return (
        <AuthContext.Provider
            value={{
                loginStatusState,
                userState,
                login,
                logout,
            }}
        >
            {props.children}
        </AuthContext.Provider>
    );
};

const fetchLogin = (email, password) =>
    fetch("/login", {
        method: "POST",
        body: JSON.stringify({ email, password }),
        headers: {
            Accept: "application/json, text/plain",
            "Content-Type": "application/json",
        },
    }).then((res) => res.json());
